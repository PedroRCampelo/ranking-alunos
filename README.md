# ranking-alunos
Script que faz o ranking dos 3 melhores notas de um vestibular.

## .SORT

O motivo da utilização da função .sort é que a mesma faz uma ordenação automática. 
Nem sempre é indicado utilizar o .sort sem uma outra função de parametro, visto que o sort faz a classificação baseada no Unicode dos elementos.

## A função de parametro tem o objetivo de: 
O que a função retornar decidirá o que vem primeiro, A ou B.

Três cenários:
- Se a função retornar um valor MENOR que 0, A vem primeiro;
- Se a função retornar um valor IGUAL a 0, nada acontece;
- Se a função retorar um valor MAIOR que 0, B vem primeiro;

A função retornou em ordem decrescente, por este motivo, inverti a ordem do return para:
n.nota - a.nota

t